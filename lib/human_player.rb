class HumanPlayer
attr_accessor :mark, :name, :board

  def initialize(name)
    @name = name
  end

  def get_move
    print "where"
    input = gets.chomp
    input.split(", ").map! {|num| num.to_i }
  end

  def display(board)
    @board = board
  end

end
