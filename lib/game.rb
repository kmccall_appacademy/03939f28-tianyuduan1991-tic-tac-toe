require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require "byebug"

class Game
attr_accessor :player_one, :player_two, :board, :mark

  def initialize(player_one, player_two)
    @board = Board.new
    @player_one = HumanPlayer.new(player_one)
    @player_two = ComputerPlayer.new(player_two)
    @current_player = @player_one.name
    @player_one.mark = :X
    @player_two.mark = :O
  end

  def mark=(mark)
    @mark = mark
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player_one.name
      @current_player = @player_two.name
    else
      @current_player = @player_one.name
    end
  end

  def play_turn
    next_move = @current_player.get_move

    until @board.empty?(next_move)
      puts "The move was invalid, please try again."
      next_move = @current_player.get_move
    end

    @board.place_mark(next_move, @current_player.mark)
    switch_players!
  end

  def play
    board.render
    until board.over?
      play_turn
    end

    if board.winner.nil?
      puts "No winner, let's try again!"
    else
      switch_players!
      puts "#{@current_player.name} is the winner!"
    end
  end

end

if $PROGRAM_NAME == __FILE__
  game = Game.new("one", "two")
  game.play
end
